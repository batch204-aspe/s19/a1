let username = prompt("Enter Your Username");
let password = prompt("Enter Your Password");
let role = prompt("Enter Your Role:");

function login(){

	if ( (username === "" || username === null) || (password === "" || password === null) || (role === "" || role === null) ){
			alert("Input Should not be empty! Please try Again");
	}

	else if (role.toLowerCase() === 'student'){
		checkAverage(92,93,96,98);
	}

	else {
		
		switch (role.toLowerCase()) {
			case 'admin':
				alert("Welcome Back to the Class Portal, Admin!");
				break;
			case 'teacher':
				alert("Thank You for logging in, Teacher!");
				break;
			case 'student':
				alert("Welcome to the Class Portal, Student!");
				break;
			default:
				alert("Role out of range");
				break;
		}
	}
}
login();


function checkAverage(grade1, grade2,grade3,grade4){

	let average = (grade1 + grade2 + grade3 + grade4)/4;
	let averageRoundOff = Math.round(average);


	if(average <= 74 ){
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is F.");
	}
	else if (average >= 75 && average <= 79) {
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is D.");
	}
	else if (average >= 80 && average <= 84) {
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is C.");
	}
	else if (average >= 85 && average <= 89) {
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is B.");
	}
	else if (average >= 90 && average <= 95){
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is A.");
	}
	else {
		console.log("Hello Student, your average is " + averageRoundOff + ". The letter equivalent is A+.");
	}

}
//checkAverage(92,93,96,98);